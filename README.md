Wassup 49ers, Android application Version Read Me
============================================


This file contains
              - A manifest for the projects, all sub-modules and libraries
              - Descriptions of the projects, all sub-modules and libraries
              - Instructions to install, configure, and run the programs
              - Known bugs
        


A manifest for the projects, all sub-modules and libraries
=====================================================================
Manifest for our project:


<?xml version="1.0" encoding="utf-8"?>
<manifest xmlns:android="http://schemas.android.com/apk/res/android"
   package="com.ssdi.wassup49ers"
   android:versionCode="1"
   android:versionName="1.0" >

   <        android:minSdkVersion="14"
       android:targetSdkVersion="17" />

   <uses-feature
       android:glEsVersion="0x00020000"
       android:required="true" />

   <uses-permission android:name="android.permission.INTERNET" />
   <uses-permission android:name="android.permission.ACCESS_NETWORK_STATE" />
   <uses-permission android:name="android.permission.WRITE_EXTERNAL_STORAGE" />
   <uses-permission android:name="android.permission.READ_CALENDAR" />
   <uses-permission android:name="android.permission.WRITE_CALENDAR" />
   <!--
     The following two permissions are not required to use
     Google Maps Android API v2, but are recommended.
    -->
   <uses-permission android:name="android.permission.ACCESS_COARSE_LOCATION" />
   <uses-permission android:name="android.permission.ACCESS_FINE_LOCATION" />
   <uses-permission android:name="android.permission.GET_ACCOUNTS" >
   </uses-permission>
   <uses-permission android:name="android.permission.AUTHENTICATE_ACCOUNTS" >
   </uses-permission>
   <uses-permission android:name="android.permission.SEND_SMS" />
   <uses-permission android:name="android.permission.WAKE_LOCK" />
   <uses-permission android:name="android.permission.RECEIVE_BOOT_COMPLETED" />
   <uses-permission android:name="android.permission.VIBRATE" />
   <uses-permission android:name="android.permission.GET_ACCOUNTS" />
   <uses-permission android:name="com.google.android.c2dm.permission.RECEIVE" />

   <!--
  IMPORTANT: Change "com.parse.tutorials.pushnotifications.permission.C2D_MESSAGE" in the lines below
  to match your app's package name + ".permission.C2D_MESSAGE".
    -->
   <permission
       android:name="com.ssdi.wassup49ers.permission.C2D_MESSAGE"
       android:protectionLevel="signature" />

   <uses-permission android:name="com.ssdi.wassup49ers.permission.C2D_MESSAGE" />

   <application
       android:name=".Wassup49ersApplication"
       android:allowBackup="true"
       android:icon="@drawable/moodleimageicon"
       android:label="@string/app_name"
       android:theme="@style/CustomActionBarTheme" >
       <meta-data
           android:name="com.google.android.gms.version"
           android:value="@integer/google_play_services_version" />

       <activity
           android:name=".MainActivity"
           android:label="@string/app_name" >
           <intent-filter>
               <action android:name="android.intent.action.MAIN" />

               <category android:name="android.intent.category.LAUNCHER" />
           </intent-filter>
       </activity>
       <activity
           android:name=".CourseWorkActivity"
           android:label="@string/title_activity_course_work" >
       </activity>
       <activity
           android:name=".StudentMenuActivity"
           android:label="@string/title_activity_menu" >
       </activity>
       <activity
           android:name=".ScheduleMeetingActivity"
           android:label="@string/title_activity_schedule_meeting" >
       </activity>
       <activity
           android:name=".EditMeetingActivity"
           android:label="@string/title_activity_edit_meeting" >
       </activity>
       <activity
           android:name=".EmergencyActivity"
           android:label="@string/title_activity_emergency" >
       </activity>
       <activity
           android:name=".ViewSlotsActivity"
           android:label="@string/title_activity_view_slots" >
       </activity>
SignUpActivity
       <activity
           android:name=".FacebookLoginActivity"
           android:label="@string/title_activity_facebook" >
       </activity>
       <activity
           android:name="com.facebook.LoginActivity"
           android:label="@string/app_name" />

       <meta-data
           android:name="com.facebook.sdk.ApplicationId"
           android:value="@string/facebook_app_id" />

       <activity
           android:name=".SignUpActivity"
           android:label="@string/title_activity_facebook" >
       </activity>
       <activity
           android:name=".RegisterEmergencyContactsActivity"
           android:label="@string/title_activity_register_emergency_contacts" >
       </activity>
       <activity
           android:name=".AddEmergencyContactActivity"
           android:label="@string/title_activity_add_emergency_contact" >
       </activity>
       <activity
           android:name=".EmergencyContactDetailsActivity"
           android:label="@string/title_activity_emergency_contact_details" >
       </activity>
       <activity
           android:name=".FacultyMenuActivity"
           android:label="@string/title_activity_faculty_menu" >
       </activity>
       <activity
           android:name=".UpdateClassRoomLocationMapActivity"
           android:label="@string/title_activity_update_classroom" >
       </activity>

       <meta-data
           android:name="com.google.android.maps.v2.API_KEY"
           android:value="AIzaSyC0EhMiSFBh17iIlAiTlrgqPbU-9f_Edn4" />

       <activity
           android:name=".UpdateClassroomLocationActivity"
           android:label="@string/title_activity_update_classroom_location" >
       </activity>

       <service android:name="com.parse.PushService" />

       <receiver android:name="com.parse.ParseBroadcastReceiver" >
           <intent-filter>
               <action android:name="android.intent.action.BOOT_COMPLETED" />
               <action android:name="android.intent.action.USER_PRESENT" />
           </intent-filter>
       </receiver>
       <receiver
           android:name="com.parse.GcmBroadcastReceiver"
           android:permission="com.google.android.c2dm.permission.SEND" >
           <intent-filter>
               <action android:name="com.google.android.c2dm.intent.RECEIVE" />
               <action android:name="com.google.android.c2dm.intent.REGISTRATION" />

               <!-- IMPORTANT: Change "com.parse.tutorials.pushnotifications" to match your app's package name. -->
               <category android:name="com.ssdi.wassup49ers" />
           </intent-filter>
       </receiver>
       <receiver
           android:name=".PushNotificationReceiver"
           android:exported="false" >
           <intent-filter>
               <action android:name="com.parse.push.intent.RECEIVE" />
               <action android:name="com.parse.push.intent.DELETE" />
               <action android:name="com.parse.push.intent.OPEN" />
           </intent-filter>
       </receiver>

       <activity
           android:name=".LocateClassroomMapActivity"
           android:label="@string/title_activity_locate_classroom_map" >
       </activity>
   </application>

</manifest>
=====================================================================






Descriptions of the projects, all sub-modules and libraries
=====================================================================
The application has the following elements:
<manifest> - The root element of the AndroidManifest.xml file. It must contain <application> element and specify xmlns:android and package attributes. 
Attributes: 
        xmlns:android : It defines the Android namespace. 
        package: It is a full Java-language-style package name for the application. The name should be unique. The name may contain uppercase or lowercase letters ('A' through 'Z'), numbers, and underscores ('_'). However, individual package name parts may only start with letters.
      android:versionCode : It is an internal version number. This number is used only to determine whether one version is more recent than another, with higher numbers indicating more recent versions. This is not the version number shown to users; that number is set by the versionName attribute.The value must be set as an integer, such as "100". You can define it however you want, as long as each successive version has a higher number. 
         android:versionName : The version number shown to users. This attribute can be set as a raw string or as a reference to a string resource. The string has no other purpose than to be displayed to users. The versionCode attribute holds the significant version number used internally.


<uses-sdk> - It is contained in <manifest>. Lets us specify the application's compatibility with one or more versions of the Android platform. It  specifies the API Level, not  the version number of the SDK. It has the attributes:


         android:minSdkVersion : It specifies the minimum API Level required for the application to run.
          android:targetSdkVersion : It is the API Level that the application targets. If not set, the default value equals that given to minSdkVersion.
<uses-feature> - Declares a single hardware or software feature that is used by the application. It is contained in <manifest>. It has the attributes:
          android:glEsVersion : The OpenGL ES version required by the application. The higher 16 bits represent the major number and the lower 16 bits represent the minor number.
        android:required : When you declare "android:required="true" for a feature, you are specifying that the application cannot function, or is not designed to function, when the specified feature is not present on the device.
<uses-permission> - Requests a permission that the application must be granted in order for it to operate correctly. Permissions are granted by the user when the application is installed, not while it's running.
<permission> - Declares a security permission that can be used to limit access to specific components or features of this or other applications. It has the following attributes:
       android:name : The name of the permission. This is the name that will be used in code to refer to the permission. 
       android:protectionLevel : "signature" : A permission that the system grants only if the requesting application is signed with the same certificate as the application that declared the permission.
<application> - This is the declaration of the application. This element contains subelements that declare each of the application's components and has attributes that can affect all the components. It has the following attributes:
        android:name : The fully qualified name of an application. 
     android:allowBackup : Whether to allow the application to participate in the backup and restore infrastructure. If this attribute is set to false, no backup or restore of the application will ever be performed, even by a full-system backup that would otherwise cause all application data to be saved via adb. The default value of this attribute is true.
      android:label : A user-readable label for the application as a whole, and a default label for each of the application's components.
      android:icon : An icon for the application as whole, and the default icon for each of the application's components. 


<meta-data> - A name-value pair for an item of additional, arbitrary data that can be supplied to the parent component. A component element can contain any number of subelements.  It has the following attributes: 
         android:name : A unique name for the item. To ensure that the name is unique, use a Java-style naming convention.
          android:value : The value assigned to the item. The data types that can be assigned as values. 
<activity> - Declares an activity (an Activity subclass) that implements part of the application's visual user interface. All activities must be represented by elements in the manifest file. Any that are not declared there will not be seen by the system and will never be run.
       android:label : A user-readable label for the activity. The label is displayed on-screen   when the activity must be represented to the user.
            android:name: The name of the class that implements the activity, a subclass of Activity.
<intent-filter> - Specifies the types of intents that an activity, service, or broadcast receiver can respond to. An intent filter declares the capabilities of its parent component i.e, what an activity or service can do and what types of broadcasts a receiver can handle.
<action> -  Adds an action to an intent filter. An <intent-filter> element must contain one or more <action> elements. If it doesn't contain any, no Intent objects will get through the filter. 
                     android:name : The name of the action. 
<service> - Declares a service (a Service subclass) as one of the application's components. Unlike activities, services lack a visual user interface. They're used to implement long-running background operations or a rich communications API that can be called by other applications.
<receiver> - Declares a broadcast receiver (a BroadcastReceiver subclass) as one of the application's components. Broadcast receivers enable applications to receive intents that are broadcast by the system or by other applications, even when other components of the application are not running.
=====================================================================






Instructions to install, configure, and run the programs
=====================================================================
There are two ways to install the application
1. Install the Wassup49ers_Final.apk
2. Build the application from the source code .
To run Wassup49ers, you can build the code.

The first step is to set up the development environment. 

==========
ANDROID SDK and ECLIPSE IDE download and install
==========

* To build this application we need to have the development environment set up. For this
follow the following steps

1. Download the Android SDK.
  Android SDK bundle can be downloaded from http://developer.android.com/sdk/index.html
        
2. Install the ADT plugin for Eclipse.
        To set up the ADT Bundle:
        - Unpack the ZIP file (named adt-bundle-<os_platform>.zip) and save it to an appropriate location, such as a "Development" directory in your home directory.
        - Open the adt-bundle-<os_platform>/eclipse/ directory and launch Eclipse.
        Caution: Do not move any of the files or directories from the adt-bundle-<os_platform> directory. If you move the eclipse/ or sdk/ directory, ADT will not be able to locate the SDK and you'll need to manually update the ADT preferences.


3. Download the latest SDK tools and platforms using the SDK Manager.
You can launch the SDK Manager in the of the following ways:
From Eclipse (with ADT), select Window > Android SDK Manager.
From Android Studio, select Tools > Android > SDK Manager.
On Windows, double-click the SDK Manager.exe file at the root of the Android SDK directory.
On Mac or Linux, open a terminal and navigate to the tools/ directory in the Android SDK, then execute android sdk.
To create a new Project in eclipse,
Click New > Android > Android Application Project > Next
 Enter all the required details. 
To importing existing projects into workspace, copy the project to your workspace then,
File > Import > General > Existing Projects into workspace > Next > Browse for the directory > Finish.
Running Android Application:
You create an Android Virtual Device(AVD) in eclipse. An AVD is an emulator that allows you to model different designs
To create an AVD:
1. Launch AVD Manager. In eclipse( Toolbar > AVD Manager)
2. AVD Manager panel > New > Create AVD
To run from eclipse,
 Open Project and click Run from Toolbar. In Run Window Android Application > Ok.


   For our project, we have used Genymotion as the emulator for app testing and presentation as it is much more faster in loading and booting. 


 Genymotion can be downloaded from
                    https://cloud.genymotion.com/page/launchpad/download/
          Follow the installation steps.
 
 To Use Genymotion,
          Open from desktop > Select a new virtual device > Sign in with credentials > Create a  new virtual device > Finish. 

========
DATABASE
========
The database used for the application is completely online in Parse.com. You need to create an account on Parse.com using this link, https://parse.com/.After creating an account, initialize the application with the client key and application id. Use the Parse Libraries to connect to parse.

* Once the development environment is set up, import the source code to eclipse IDE.
1.Build the FacebookSDK library
2.Build the google-play-services_lib library
3.Link the above two libraries for the project.
4.Build and run the wassup49ers source code.


=====================================================================



Known bugs
=====================================================================


1. The notifications for scheduling a group meeting is slightly delayed. 
2.The emergency notifications do not come with the GPS co-ordinates.
3.Feature Request : The “Sync Course Calendar” is enabled even when the user has synced once with the course timings.
4.Feature Request: Emergency contacts should able to be added from the Phone Contacts.