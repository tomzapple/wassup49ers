package com.ssdi.wassup49ers;

import java.util.List;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;

public class AddEmergencyContactActivity extends Activity {
	String name, number;
	ParseUser currentUser;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_add_emergency_contact);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.add_emergency_contact, menu);
		return true;
	}

	public void onSaveClick(View v) {

		EditText nameEditText = (EditText) findViewById(R.id.editText1);
		EditText numberEditText = (EditText) findViewById(R.id.editText2);

		name = nameEditText.getText().toString();
		number = numberEditText.getText().toString();

		if (nameEditText.getText().toString().equals("")
				|| numberEditText.getText().toString().equals("")) {
			Toast.makeText(this, "Please enter into the missing text field",
					Toast.LENGTH_SHORT).show();
		} else {

			currentUser = ParseUser.getCurrentUser();
			if (currentUser != null) {

				ParseQuery<ParseObject> query = ParseQuery
						.getQuery("EmergencyContact");
				query.whereEqualTo("user", currentUser);
				query.findInBackground(new FindCallback<ParseObject>() {
					@Override
					public void done(List<ParseObject> userEmergencyContacts,
							ParseException e) {
						if (e == null) {

						} else {

						}

						ParseObject emergencyContact = new ParseObject(
								"EmergencyContact");
						emergencyContact.put("name", name);
						emergencyContact.put("number", number);
						emergencyContact.put("user", currentUser);
						emergencyContact.saveInBackground(new SaveCallback() {

							@Override
							public void done(ParseException e) {
								Toast.makeText(
										AddEmergencyContactActivity.this,
										"Emergency contacts successfully added",
										Toast.LENGTH_SHORT).show();

							}
						});

					}

				});

				finish();
			}
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
