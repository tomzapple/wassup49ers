package com.ssdi.wassup49ers;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class CourseWorkActivity extends Activity {
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_course_work);
		this.setTitle(R.string.Coursework_Title);

	}
	
	public void onScheduleAMeetingClicked(View v){
		Intent intent = new Intent(CourseWorkActivity.this,
				ScheduleMeetingActivity.class);
		startActivity(intent);
		
	}
	
	public void onViewMeetingClicked(View v){
//		Intent calIntent = new Intent(Intent.ACTION_VIEW);
//		calIntent.setType("vnd.android.cursor.item/event");
//		
//		Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("EST"));
//
//		calIntent.putExtra("beginTime", cal.getTimeInMillis());
//		 cal.add(Calendar.MONTH, -1);
//		calIntent.putExtra("endTime", cal);      
//
//
//		startActivity(calIntent);
		Intent calIntent = new Intent(Intent.ACTION_EDIT);
		calIntent.setType("vnd.android.cursor.item/event");

		startActivity(calIntent);

		
	}
	
	public void onEditMeetingClicked(View v) {

		Intent calIntent = new Intent(Intent.ACTION_EDIT);
		calIntent.setType("vnd.android.cursor.item/event");

		startActivity(calIntent);

	}
	
	public void onLocateClassroomClicked(View v){
		
		Intent intent = new Intent(CourseWorkActivity.this,
				LocateClassroomMapActivity.class);
		startActivity(intent);
	}
	
}
