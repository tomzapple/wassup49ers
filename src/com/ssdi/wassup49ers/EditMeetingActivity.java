package com.ssdi.wassup49ers;

import android.app.Activity;
import android.os.Bundle;

public class EditMeetingActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_edit_meeting);
		this.setTitle(R.string.Editmeeting_Title);
	}
}
