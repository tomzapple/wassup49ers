package com.ssdi.wassup49ers;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

public class EmergencyActivity extends Activity {
	static final String PARSE_EMERGENCY_CONTACT = "EmergencyContact";
	static final String EMERGENCY_CONTACT_NAME_KEY_EXTRAS = "EMERGENCY_CONTACT_NAME_KEY_EXTRAS";
	static final String EMERGENCY_CONTACT_NUMBER_KEY_EXTRAS = "EMERGENCY_CONTACT_NUMBER_KEY_EXTRAS";
	
	ParseUser currentUser;
	ArrayList<EmergencyContact> emergencyContactList;
	ArrayList<String> emergencyContactNames;
	LocationManager mLocationManager;
	LocationListener mLocationListener;
	
	Location currentLocation;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_emergency);
		this.setTitle(R.string.Emergency_Title);

		mLocationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
		checkForLocationServicesEnabled();
		

		currentUser = ParseUser.getCurrentUser();
		

		// Check if the Emergency Array is created on Parse
		ParseQuery<ParseObject> query = ParseQuery.getQuery("EmergencyContact");
		query.whereEqualTo("user", currentUser);
		query.findInBackground(new FindCallback<ParseObject>() {
			@Override
			public void done(List<ParseObject> emergencyContacts,
					ParseException e) {
				if (e == null) {
					Log.d("demo",
							"Emergency contacts " + emergencyContacts.size()
									+ " events");
					// eventList = eventsList;
				} else {
					Log.d("demo", "Error: " + e.getMessage());
					// eventList = new ParseObject("Events");

				}

				emergencyContactNames = new ArrayList<String>();
				emergencyContactList = new ArrayList<EmergencyContact>();

				for (ParseObject object : emergencyContacts) {
					try {
						object.fetchIfNeeded();

						emergencyContactNames.add((String) object.get("name"));
						emergencyContactList.add(new EmergencyContact(
								(String) object.get("number"), (String) object
										.get("name")));
					} catch (ParseException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}

				}

				ListView listview = (ListView) findViewById(R.id.listView1);

				ArrayAdapter<String> adapter = new ArrayAdapter<String>(
						EmergencyActivity.this,
						android.R.layout.simple_list_item_1,
						emergencyContactNames);

				listview.setAdapter(adapter);

				adapter.setNotifyOnChange(true);

				listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {

					@Override
					public void onItemClick(AdapterView<?> parent, View view,
							int position, long id) {

						Intent intent = new Intent(parent.getContext(),
								EmergencyContactDetailsActivity.class);
						intent.putExtra(EMERGENCY_CONTACT_NAME_KEY_EXTRAS,
								emergencyContactNames.get(position));
						intent.putExtra(EMERGENCY_CONTACT_NUMBER_KEY_EXTRAS,
								emergencyContactList.get(position).number);

						parent.getContext().startActivity(intent);
					}

				});

			}

		});
	}

	public void onAddEmergencyContactClick(View v) {
		Intent intent = new Intent(EmergencyActivity.this,
				AddEmergencyContactActivity.class);
		startActivity(intent);
	}

	public void onHelpButtonClick(View v) {
		if (emergencyContactList != null && emergencyContactList.size() < 1) {
			Toast.makeText(EmergencyActivity.this,
					"Please register for Emergency numbers", Toast.LENGTH_SHORT)
					.show();
		} else {
			
			new AlertDialog.Builder(this)
		    .setTitle("Requesting Emergency")
		    .setMessage("Are you sure you want to send Emergency requests to all the Emergency contacts? Remember your emergency situation will be notified to the Campus Police and the Charlotte Police Department")
		    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
		        public void onClick(DialogInterface dialog, int which) { 
		        	String messageToSend ;
		        	if(currentLocation == null){
					 messageToSend = "Hey I am in an emergency situation ! Please Help Me. ";
		        	}else{
		        		 messageToSend = "Hey I am in an emergency situation ! Please Help Me ! My coordinates are Longtitude:"+currentLocation.getLongitude()+" Latitude:"+currentLocation.getLatitude();
		        	}
					 String number = "12676328580;";
//		        	for(EmergencyContact contact : emergencyContactList){
//		        		number += contact.number;
//		        		number += ";";
//		        	}
			        	for(EmergencyContact contact : emergencyContactList){
		        		number = contact.number;
		        		SmsManager.getDefault().sendTextMessage(number, null, messageToSend, null,null);
		        	}



					 
		        }
		     })
		    .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
		        public void onClick(DialogInterface dialog, int which) { 
		            // do nothing
		        }
		     })
		    .setIcon(android.R.drawable.ic_dialog_alert)
		     .show();
			Toast.makeText(EmergencyActivity.this, "Help Requested",
					Toast.LENGTH_SHORT).show();

		}

	}
	
	private void checkForLocationServicesEnabled() {

		if(!mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)){
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setTitle("GPS Not Enabled")
			.setMessage("Would you like to enable GPS from settings")
			.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
					startActivity(intent);
					
				}
			})
			.setNegativeButton("No", new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					// TODO Auto-generated method stub
					dialog.cancel();
					//finish();
				}
			});
			AlertDialog alert = builder.create();
			alert.show();
				

		}else{
			mLocationListener = new LocationListener() {
				@Override
				public void onStatusChanged(String provider, int status, Bundle extras) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void onProviderEnabled(String provider) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void onProviderDisabled(String provider) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void onLocationChanged(final Location location) {
					
					final Wassup49ersApplication application = (Wassup49ersApplication) getApplication();

					Log.d("demo", location.getLatitude() + "," +location.getLongitude() );
					
					currentLocation = location;
			

//					walk.put("user", application.);
					
					
				}
			};
			Wassup49ersApplication application = (Wassup49ersApplication) getApplication();
			application.setLocationMngr(mLocationManager) ;
			application.setLocListener(mLocationListener) ;
			
			mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, mLocationListener);
		}
	}
	
	@Override
	protected void onResume() {
		checkForLocationServicesEnabled();
		
		super.onResume();
	}


}
