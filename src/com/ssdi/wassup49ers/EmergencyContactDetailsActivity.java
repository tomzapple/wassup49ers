package com.ssdi.wassup49ers;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.parse.ParseObject;
import com.parse.ParseUser;

public class EmergencyContactDetailsActivity extends Activity {
	ParseUser currentUser ;
	String emergencyContactName;
	String emergencyContactNumber;
	ParseObject currentObject;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_emergency_contact_details);

		// fetch from extras the name
		emergencyContactName = getIntent().getStringExtra(
				EmergencyActivity.EMERGENCY_CONTACT_NAME_KEY_EXTRAS);
		emergencyContactNumber = getIntent().getStringExtra(EmergencyActivity.EMERGENCY_CONTACT_NUMBER_KEY_EXTRAS);

		TextView emergencyContactNameTv = (TextView) findViewById(R.id.textView2);
		emergencyContactNameTv.setText(emergencyContactName);

		TextView emergencyContactNumberTv = (TextView) findViewById(R.id.textView4);
		emergencyContactNumberTv.setText(emergencyContactNumber);

	}


}
