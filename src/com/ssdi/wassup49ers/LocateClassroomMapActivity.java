package com.ssdi.wassup49ers;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseGeoPoint;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

public class LocateClassroomMapActivity extends Activity {
	GoogleMap gMap;
	LatLng classRoomLocation;
	Marker classMarker;
	LatLng currentSelectedLocation;
	
	ArrayList<ParseObject>courseList;
	ArrayList<String> courseNames;
	
	ArrayList<Marker> markerList;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_locate_classroom_map);
		
		setUpMapIfNeeded();
		
		ParseQuery<ParseObject> query = ParseQuery.getQuery("Student");
		query.whereEqualTo("userId", ParseUser.getCurrentUser());
		query.getFirstInBackground(new GetCallback<ParseObject>() {

			@Override
			public void done(ParseObject object, ParseException e) {
				ArrayList<String> courseIDs = (ArrayList<String>) object
						.get("courses");

				ParseQuery<ParseObject> innerQuery = ParseQuery
						.getQuery("Course");
				innerQuery.whereContainedIn("objectId", courseIDs);
				innerQuery.findInBackground(new FindCallback<ParseObject>() {

					@Override
					public void done(List<ParseObject> objects, ParseException e) {
						courseList = (ArrayList<ParseObject>) objects;
						courseNames = new ArrayList<String>();
						for (ParseObject obj: courseList){
							courseNames.add(obj.getString("courseName"));
						}
						Spinner courseSpinner = (Spinner) findViewById(R.id.Spinner1);
						ArrayAdapter<String> adapter = new ArrayAdapter<String>(
								LocateClassroomMapActivity.this,
								android.R.layout.simple_spinner_item,
								courseNames);
						adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
						// Apply the adapter to the spinner
						courseSpinner.setAdapter(adapter);
						markerList = new ArrayList<Marker>();
						
						zoomMapForClassLocation(new LatLng(((ParseGeoPoint) courseList.get(0).get("gpsLocation")).getLatitude(), ((ParseGeoPoint) courseList.get(0).get("gpsLocation")).getLongitude()));
						Marker marker = gMap.addMarker(new MarkerOptions().title(courseNames.get(0)
								).snippet(courseList.get(0).getString("location"))
								.position(new LatLng(((ParseGeoPoint) courseList.get(0).get("gpsLocation")).getLatitude(), ((ParseGeoPoint) courseList.get(0).get("gpsLocation")).getLongitude())));
						markerList.add(marker);
						marker.showInfoWindow();
						
						courseSpinner
						.setOnItemSelectedListener(new OnItemSelectedListener() {

							@Override
							public void onItemSelected(AdapterView<?> parent,
									View view, int position, long id) {
								
								for (Marker mk: markerList){
									mk.remove();
								}
								
								ParseObject courseObj = courseList.get(position);
								zoomMapForClassLocation(new LatLng(((ParseGeoPoint) courseObj.get("gpsLocation")).getLatitude(), ((ParseGeoPoint) courseObj.get("gpsLocation")).getLongitude()));
								Marker marker = gMap.addMarker(new MarkerOptions().title(courseNames.get(position)
										).snippet(courseObj.getString("location"))
										.position(new LatLng(((ParseGeoPoint) courseObj.get("gpsLocation")).getLatitude(), ((ParseGeoPoint) courseObj.get("gpsLocation")).getLongitude())));
								markerList.add(marker);
								marker.showInfoWindow();
								
							}

							@Override
							public void onNothingSelected(AdapterView<?> parent) {
								// TODO Auto-generated method stub
								
							}
						});

					}
				});
			}
		});
		
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.update_classroom, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	private LatLngBounds.Builder getZooomedLatLngBoundsForGeoPoints(ArrayList<LatLng> points){
	    LatLngBounds.Builder latLngBounds = new LatLngBounds.Builder();

	    for (LatLng item : points) {
	        latLngBounds.include(item);
	    }
	    return latLngBounds;
	}
	private void setUpMapIfNeeded() {
	    // Do a null check to confirm that we have not already instantiated the map.
	    if (gMap == null) {
	    	gMap = ((MapFragment) getFragmentManager().findFragmentById(R.id.map))
	                            .getMap();
	    	
	    }
	}
	
	
	private void zoomMapForClassLocation(LatLng classLocation){
		final ArrayList<LatLng> classroomLocationArrayList = new ArrayList<LatLng>();
		classroomLocationArrayList.add(classLocation);
		
		gMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
			@Override
			public void onMapLoaded() {
				// zoom and pan the map to include all the place points
				gMap.moveCamera(CameraUpdateFactory.newLatLngBounds(
						getZooomedLatLngBoundsForGeoPoints(classroomLocationArrayList)
								.build(), 20));
			}
		});
	}
	

	

}
