package com.ssdi.wassup49ers;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.Signature;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.LogInCallback;
import com.parse.ParseException;
import com.parse.ParseInstallation;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;

public class MainActivity extends Activity {


	EditText userNameEditText;
	EditText passwordEditText;

	String selectedAccount;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		this.setTitle(R.string.MainActivity_Title);
		
		


		String[] possibleEmails = { "" };

		ParseUser currentUser = ParseUser.getCurrentUser();
		if (currentUser != null) {

			// get the google account of the for use by calendar
			AccountManager manager = AccountManager.get(this);
			Account[] accounts = manager.getAccountsByType("com.google");
			int i = 0;
			for (Account account : accounts) {
				// TODO: Check possibleEmail against an email regex or treat
				// account.name as an email address only for certain
				// account.type
				// values.

				possibleEmails[i] = account.name;
			}
			if (i > 1) {
				chooseAnAccount(possibleEmails);

			} else {
				selectedAccount = possibleEmails[0];
			}

			ParseQuery<ParseObject> queryGoogleAccount = new ParseQuery<ParseObject>("Calendar");
			queryGoogleAccount.whereEqualTo("userId", currentUser);
			queryGoogleAccount.whereExists("googleAccount");
			queryGoogleAccount.getFirstInBackground(new GetCallback<ParseObject>() {
				
				@Override
				public void done(ParseObject object, ParseException e) {
					final Wassup49ersApplication app = (Wassup49ersApplication) getApplication();
					if (object != null){
						Log.d("demo", "Already has the google account synced");
						app.setGoogleAccount(object.getString("googleAccount"));
						
					}else{
						ParseObject calendar = new ParseObject("Calendar");
						calendar.put("googleAccount", selectedAccount);
						calendar.put("userId", ParseUser.getCurrentUser());

						calendar.saveInBackground(new SaveCallback() {

							@Override
							public void done(ParseException e) {
								// TODO Auto-generated method stub
								Log.d("demo", "Users google account saved");
								
								app.setGoogleAccount(selectedAccount);
							}
						});
						
					}
					
				}
			});

			showMenuForUser(currentUser);

		} else {
			// show the signup or login screen
			Log.d("demo", "user not authenticated");
		}

	}

	// Login button method
	public void onLoginButtonClick(View view) {
		userNameEditText = (EditText) findViewById(R.id.EditText_Username);
		passwordEditText = (EditText) findViewById(R.id.Edittext_Password);

		ParseUser user = new ParseUser();

		String userName = userNameEditText.getText().toString();
		user.setUsername(userName);

		String password = passwordEditText.getText().toString();
		user.setPassword(password);

		user.setEmail(userName);

		ParseUser.logInInBackground(userName, password, new LogInCallback() {
			public void done(final ParseUser user, ParseException e) {
				showMenuForUser(user);
			}
		});

	}

	public void onSignUpButtonClick(View v) {
		Intent intent = new Intent(MainActivity.this, SignUpActivity.class);
		startActivity(intent);
	}

	private void chooseAnAccount(final String[] accounts) {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle("Choose a Google Account").setItems(accounts,
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						selectedAccount = (String) accounts[which];
						Log.d("demo", "selected is " + selectedAccount);
						// placeType.setText(selectedPlaceType);
					}
				});
		final AlertDialog alert = builder.create();
		alert.show();

	}

	private void showMenuForUser(final ParseUser user) {
		if (user != null) {
			
			
			//get the username and save it in the Application object
			Wassup49ersApplication app = (Wassup49ersApplication) getApplication();
			app.setUserName(user.getString("userFullName"));
			
		    // Add code to print out the key hash
		    try {
		        PackageInfo info = getPackageManager().getPackageInfo(
		                "com.ssdi.wassup49ers", 
		                PackageManager.GET_SIGNATURES);
				for (Signature signature : info.signatures) {
					MessageDigest md = MessageDigest.getInstance("SHA");
					md.update(signature.toByteArray());
					String keyHash = Base64.encodeToString(md.digest(),
							Base64.DEFAULT);
					Log.d("KeyHash:", keyHash);
//					Toast.makeText(this, "keyhash:"+keyHash, Toast.LENGTH_LONG).show();
				}
		    } catch (NameNotFoundException e) {

		    } catch (NoSuchAlgorithmException e) {

		    }
			
			// Associate the device with a user for push notification
			ParseInstallation installation = ParseInstallation
					.getCurrentInstallation();
			installation.put("user", user);
			installation.saveInBackground();
			
			final ArrayList<ParseUser> parseUserList = new ArrayList<ParseUser>();
			parseUserList.add(user);
			ParseQuery<ParseObject> query1 = ParseQuery.getQuery("Student");
			query1.whereContainedIn("userId", parseUserList);
			query1.findInBackground(new FindCallback<ParseObject>() {
				@Override
				public void done(List<ParseObject> objects, ParseException e) {
					if (objects.size() > 0) {
						String userObjectId =  ((ParseObject) objects.get(0).get("userId")).getObjectId();
						if (userObjectId.equals(user.getObjectId())) {
							Toast.makeText(MainActivity.this,
									"Student Login Successful",
									Toast.LENGTH_SHORT).show();
							finish();

							Intent intent = new Intent(MainActivity.this,
									StudentMenuActivity.class);
							startActivity(intent);
						}
					} else {
						ParseQuery<ParseObject> query2 = ParseQuery
								.getQuery("Faculty");
						query2.whereContainedIn("userID", parseUserList);
						query2.findInBackground(new FindCallback<ParseObject>() {

							@Override
							public void done(List<ParseObject> objects,
									ParseException e) {
								if (objects.size() > 0) {
									String userObjectId =  ((ParseObject) objects.get(0).get("userID")).getObjectId();
									Log.d("demo", userObjectId);
									Log.d("demo", user.getObjectId());
									if (userObjectId.equals(user.getObjectId())) {
										Toast.makeText(MainActivity.this,
												"Faculty Login Successful",
												Toast.LENGTH_SHORT).show();
										finish();
										Intent intent = new Intent(
												MainActivity.this,
												FacultyMenuActivity.class);
										startActivity(intent);

									}
								}

							}
						});
					}

				}
			});

		} else {
			// Signup failed. Look at the ParseException to see what
			// happened.

			Toast.makeText(MainActivity.this,
					"Login was not successful ! Please retry",
					Toast.LENGTH_SHORT).show();
		}
		
	}

}
