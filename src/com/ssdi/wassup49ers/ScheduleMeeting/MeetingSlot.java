package com.ssdi.wassup49ers.ScheduleMeeting;

import java.text.SimpleDateFormat;
import java.util.Date;

public class MeetingSlot {
	Date startDate;
	Date endDate;
	
	public MeetingSlot(Date startDate, Date endDate) {
		super();
		this.startDate = startDate;
		this.endDate = endDate;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	@Override
	public String toString() {
		SimpleDateFormat df = (SimpleDateFormat) SimpleDateFormat.getDateTimeInstance(SimpleDateFormat.MEDIUM, SimpleDateFormat.MEDIUM);
		return df.format(startDate) + " to " +df.format(endDate);
		//return "MeetingSlot [startDate=" + startDate + ", endDate=" + endDate
				//+ "]";
	}
	
}
