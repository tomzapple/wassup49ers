package com.ssdi.wassup49ers;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TimePicker;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

public class ScheduleMeetingActivity extends Activity {
	ParseUser currentUser;
	ArrayList<String> courseNames;
	ArrayList<String> courseIDs;
	ArrayList<String> groupIDs;
	ArrayList<String> groupNames;
	ArrayList<ParseObject> courseList;
	ArrayList<ParseObject> groupsList;
	ParseObject studentObject;
	ParseObject currentCourse;
	ParseObject currentGroup;

	Wassup49ersApplication app;

	Date startDate;
	Date endDate;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_schedule_meeting);

		app = (Wassup49ersApplication) getApplication();
		currentUser = ParseUser.getCurrentUser();

		ParseQuery<ParseObject> query = ParseQuery.getQuery("Student");
		query.whereEqualTo("userId", currentUser);
		// Log.d("demo", currentUser.getObjectId());
		query.findInBackground(new FindCallback<ParseObject>() {

			@Override
			public void done(List<ParseObject> objects, ParseException e) {
				studentObject = (ParseObject) objects.get(0);
				courseIDs = (ArrayList<String>) studentObject.get("courses");

				groupIDs = (ArrayList<String>) studentObject.get("groups");
				courseNames = new ArrayList<String>();

				ParseQuery<ParseObject> query1 = ParseQuery.getQuery("Course");
				query1.whereContainedIn("objectId", courseIDs);
				query1.findInBackground(new FindCallback<ParseObject>() {
					@Override
					public void done(List<ParseObject> objects, ParseException e) {
						courseList = new ArrayList<ParseObject>();
						for (ParseObject obj : objects) {
							courseNames.add((String) obj.get("courseName"));
							courseList.add(obj);
						}

						Spinner courseSpinner = (Spinner) findViewById(R.id.spinner1);

						ArrayAdapter<String> adapter = new ArrayAdapter<String>(
								ScheduleMeetingActivity.this,
								android.R.layout.simple_spinner_item,
								courseNames);

						// Specify the layout to use when the list of choices
						// appears
						adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
						// Apply the adapter to the spinner
						courseSpinner.setAdapter(adapter);

						courseSpinner
								.setOnItemSelectedListener(new OnItemSelectedListener() {

									@Override
									public void onItemSelected(
											AdapterView<?> parent, View view,
											int position, long id) {

										ParseQuery<ParseObject> query = ParseQuery
												.getQuery("Group");
										Log.d("demo",
												"Course ID :"
														+ courseIDs
																.get(position));

										app.setCurrentCourse(courseNames
												.get(position));

										currentCourse = courseList
												.get(position);
										query.whereEqualTo("courseId",
												currentCourse);

										groupNames = new ArrayList<String>();
										query.findInBackground(new FindCallback<ParseObject>() {

											@Override
											public void done(
													List<ParseObject> objects,
													ParseException e) {
												groupsList = new ArrayList<ParseObject>();
												for (ParseObject obj : objects) {
													ArrayList<String> groupUsers = (ArrayList<String>) obj
															.get("members");
													for (String user : groupUsers) {
														if (user.equals(currentUser
																.getObjectId())) {
															groupsList.add(obj);
															groupNames.add(obj
																	.getString("groupName"));
														}
													}
												}
												Log.d("demo",
														groupNames.toString());

												Spinner groupSpinner = (Spinner) findViewById(R.id.spinner2);

												ArrayAdapter<String> adapter = new ArrayAdapter<String>(
														ScheduleMeetingActivity.this,
														android.R.layout.simple_spinner_item,
														groupNames);

												// Specify the layout to use
												// when the list of choices
												// appears
												adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
												// Apply the adapter to the
												// spinner
												groupSpinner
														.setAdapter(adapter);
												groupSpinner
														.setOnItemSelectedListener(new OnItemSelectedListener() {

															@Override
															public void onItemSelected(
																	AdapterView<?> parent,
																	View view,
																	int position,
																	long id) {
																// TODO
																// Auto-generated
																// method stub
																currentGroup = groupsList
																		.get(position);

																app.setCurrentGroup(groupNames
																		.get(position));

															}

															@Override
															public void onNothingSelected(
																	AdapterView<?> parent) {
																// TODO
																// Auto-generated
																// method stub

															}

														});

											}

										});

									}

									@Override
									public void onNothingSelected(
											AdapterView<?> parent) {
										// TODO Auto-generated method stub

									}
								});

					}
				});
			}
		});

		this.setTitle(R.string.SchedulemeetingActivity_Title);
	}

	public void onStartDateEditTextClicked(View view) {
		DialogFragment newFragment = new DatePickerFragment(view);
		newFragment.show(getFragmentManager(), "datePicker");
	}

	public void onStartTimeEditTextClicked(View view) {

		DialogFragment newFragment = new TimePickerFragment(view);
		newFragment.show(getFragmentManager(), "timePicker");
	}

	public void onEndDateEditTextClicked(View view) {
		DialogFragment newFragment = new DatePickerFragment(view);
		newFragment.show(getFragmentManager(), "datePicker");
	}

	public void onEndTimeEditTextClicked(View view) {

		DialogFragment newFragment = new TimePickerFragment(view);
		newFragment.show(getFragmentManager(), "timePicker");
	}

	public void onViewAvailableSlotsClick(View view) {

		EditText et1 = (EditText) findViewById(R.id.editText1);
		// et1.setText("");
		EditText et3 = (EditText) findViewById(R.id.editText3);
		// et3.setText("");

		EditText et2 = (EditText) findViewById(R.id.editText2);
		// et2.setText("");
		EditText et4 = (EditText) findViewById(R.id.editText4);
		// et4.setText("");
		EditText et5 = (EditText) findViewById(R.id.editText5);

		int duration = Integer.parseInt(et5.getText().toString());

		startDate = getDateFromString(et1.getText().toString() + " "
				+ et3.getText().toString());
		endDate = getDateFromString(et2.getText().toString() + " "
				+ et4.getText().toString());

		if (duration > calculateMinutesBetweenDate(startDate, endDate)) {
			Toast.makeText(
					this,
					"Meeting duration more than the mentioned "
							+ "start and end dates. Reenter the duration!",
					Toast.LENGTH_SHORT).show();
			et5.setText("");
		} else {

			Intent intent = new Intent(ScheduleMeetingActivity.this,
					ViewSlotsActivity.class);
			intent.putExtra("startDate", startDate.getTime());
			intent.putExtra("endDate", endDate.getTime());
			intent.putExtra("duration", duration);
			startActivity(intent);
		}
	}

	public Date getDateFromString(String dtString) {
		SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy HH:mm");

		try {
			Date date = format.parse(dtString);
			Log.d("demo", date.toString());
			return date;
		} catch (java.text.ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}

	}

	public int calculateMinutesBetweenDate(Date stDate, Date endDate) {
		long difference = endDate.getTime() - stDate.getTime();
		long differenceBack = difference;
		differenceBack = difference / 1000;
		long secs = differenceBack % 60;
		differenceBack /= 60;
		long mins = differenceBack % 60;
		differenceBack /= 60;
		long hours = differenceBack % 24;
		differenceBack /= 24;
		long days = differenceBack % 24;
		return (int) ((int) mins + hours * 60 + days * 24 * 60);
	}

	public void resetAllEditText() {
		EditText et1 = (EditText) findViewById(R.id.editText1);
		et1.setText("");
		EditText et2 = (EditText) findViewById(R.id.editText2);
		et2.setText("");
		EditText et3 = (EditText) findViewById(R.id.editText3);
		et3.setText("");
		EditText et4 = (EditText) findViewById(R.id.editText4);
		et4.setText("");
		EditText et5 = (EditText) findViewById(R.id.editText5);
		et5.setText("");
	}

	// reference
	// http://developer.android.com/guide/topics/ui/controls/pickers.html
	public static class DatePickerFragment extends DialogFragment implements
			DatePickerDialog.OnDateSetListener {
		View editText;

		public DatePickerFragment(View v) {
			editText = v;
		}

		@Override
		public Dialog onCreateDialog(Bundle savedInstanceState) {
			// Use the current date as the default date in the picker
			final Calendar c = Calendar.getInstance();
			int year = c.get(Calendar.YEAR);
			int month = c.get(Calendar.MONTH);
			int day = c.get(Calendar.DAY_OF_MONTH);

			// Create a new instance of DatePickerDialog and return it
			return new DatePickerDialog(getActivity(), this, year, month, day);
		}

		public void onDateSet(DatePicker view, int year, int month, int day) {
			// Do something with the date chosen by the user

			Calendar calendar = Calendar.getInstance();
			calendar.set(year, month, day);

			String year_string = String.valueOf(year);
			String month_string = String.valueOf(month + 1);
			String day_string = String.valueOf(day);

			((EditText) editText).setText(month_string + "/" + day_string + "/"
					+ year_string);

			if (editText.getId() == R.id.editText1) {

			} else if (editText.getId() == R.id.editText2) {

			}
		}
	}

	public static class TimePickerFragment extends DialogFragment implements
			TimePickerDialog.OnTimeSetListener {

		View editText;

		public TimePickerFragment(View view) {
			editText = view;
		}

		@Override
		public Dialog onCreateDialog(Bundle savedInstanceState) {
			// Use the current time as the default values for the picker
			final Calendar c = Calendar.getInstance();
			int hour = c.get(Calendar.HOUR_OF_DAY);
			int minute = c.get(Calendar.MINUTE);

			// Create a new instance of TimePickerDialog and return it
			return new TimePickerDialog(getActivity(), this, hour, minute,
					DateFormat.is24HourFormat(getActivity()));
		}

		public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
			// Do something with the time chosen by the user
			EditText et1 = (EditText) getActivity()
					.findViewById(R.id.editText1);
			// EditText et2 = (EditText)
			// getActivity().findViewById(R.id.editText2);
			if (et1.getText().toString().equals(" ")
					&& (editText.getId() == R.id.editText1)) {
				Toast.makeText(getActivity(), "Enter the start date first",
						Toast.LENGTH_SHORT).show();
			}/*
			 * else if(et2.getText().toString().equals(" ") && (editText.getId()
			 * == R.id.editText2)){ Toast.makeText(getActivity(),
			 * "Enter the end date first", Toast.LENGTH_SHORT).show(); }
			 */else {

				((EditText) editText).setText(hourOfDay + ":" + minute);

			}
		}
	}

}
