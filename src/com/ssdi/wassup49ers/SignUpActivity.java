// An activity to Signup the User
package com.ssdi.wassup49ers;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.SignUpCallback;

public class SignUpActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_sign_up);
	}

	public void onSignupButtonClicked(View v) {
		
		EditText fullNameEditText= (EditText) findViewById(R.id.signup_fullname_editText);
		String fullName = fullNameEditText.getText().toString();
		
		EditText emailEditText= (EditText) findViewById(R.id.signup_email_editText);
		String userName = emailEditText.getText().toString();
		
		EditText passwordEditText= (EditText) findViewById(R.id.signup_password_editText);
		String password = passwordEditText.getText().toString();
		
		ParseUser user = new ParseUser();
		user.setUsername(userName);
		user.setPassword(password);
		user.setEmail(userName);
		
		user.put("userFullName", fullName);
		 
		user.signUpInBackground(new SignUpCallback() {
			public void done(ParseException e) {
				if (e == null) {
					// Hooray! Let them use the app now.
					Log.d("demo", "login succeeded");

					Toast.makeText(SignUpActivity.this, "Signup Successful",
							Toast.LENGTH_SHORT).show();
					finish();
					Intent intent = new Intent(SignUpActivity.this,
							StudentMenuActivity.class);
					startActivity(intent);

				} else {
					// Sign up didn't succeed. Look at the ParseException
					// to figure out what went wrong
					Log.d("demo", "login failed");
					Toast.makeText(
							SignUpActivity.this,
							"Looks like there is someone with the same email id. Try Again !",
							Toast.LENGTH_SHORT).show();
				}
			}
		});

	}

	public void onCancelButtonClicked(View v) {
		finish();
		Intent intent = new Intent(SignUpActivity.this, MainActivity.class);
		startActivity(intent);

	}
}
