package com.ssdi.wassup49ers;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.CalendarContract;
import android.provider.CalendarContract.Attendees;
import android.provider.CalendarContract.Calendars;
import android.provider.CalendarContract.Events;
import android.provider.CalendarContract.Reminders;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

public class StudentMenuActivity extends Activity {

	String accountName;
	Wassup49ersApplication app;
//	String[] menuItems = { "Course Work", "Emergency", "Facebook" };
	ArrayList<ParseObject> courseList;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		app = (Wassup49ersApplication) getApplication();
		accountName = app.getGoogleAccount();
		setContentView(R.layout.activity_menu);
		this.setTitle(R.string.MenuActivity_Title);
//		ListView listView = (ListView) findViewById(R.id.listView1);
//		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
//				android.R.layout.simple_list_item_1, menuItems);
//		listView.setAdapter(adapter);
//		adapter.setNotifyOnChange(true);

		// fetch students courses

		ParseQuery<ParseObject> query = ParseQuery.getQuery("Student");
		query.whereEqualTo("userId", ParseUser.getCurrentUser());
		query.getFirstInBackground(new GetCallback<ParseObject>() {

			@Override
			public void done(ParseObject object, ParseException e) {
				ArrayList<String> courseIDs = (ArrayList<String>) object
						.get("courses");

				ParseQuery<ParseObject> innerQuery = ParseQuery
						.getQuery("Course");
				innerQuery.whereContainedIn("objectId", courseIDs);
				innerQuery.findInBackground(new FindCallback<ParseObject>() {

					@Override
					public void done(List<ParseObject> objects, ParseException e) {
						courseList = (ArrayList<ParseObject>) objects;

					}
				});
			}
		});
//		// Log.d("demo", currentUser.getObjectId());
//
//		// for the selected menu in the listview
//		listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//
//			@Override
//			public void onItemClick(AdapterView<?> parent, View view,
//					int position, long id) {
//				Log.d("demo", "Postion:" + position + " Value:"
//						+ menuItems[position].toString());
//				Intent intent;
//				switch (position) {
//				case 0:
//					intent = new Intent(StudentMenuActivity.this,
//							CourseWorkActivity.class);
//					startActivity(intent);
//					break;
//				case 1:
//					intent = new Intent(StudentMenuActivity.this,
//							EmergencyActivity.class);
//					startActivity(intent);
//					break;
//				case 2:
//					intent = new Intent(StudentMenuActivity.this,
//							FacebookLoginActivity.class);
//					startActivity(intent);
//					break;
//
//				default:
//					break;
//				}
//
//			}
//
//		});

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.add_emergency_contact, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			ParseUser.logOut();
			finish();
			Intent intent = new Intent(this, MainActivity.class);
			startActivity(intent);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	private long getLocalCalendarId() {
		String[] projection = new String[] { Calendars._ID };
		String selection = Calendars.ACCOUNT_NAME + " =? AND "
				+ Calendars.ACCOUNT_TYPE + " =?";
		// use the same values as above:
		String[] selArgs = new String[] { accountName,
				CalendarContract.ACCOUNT_TYPE_LOCAL };
		Cursor cursor = getContentResolver().query(Calendars.CONTENT_URI,
				projection, selection, selArgs, null);
		if (cursor.moveToFirst()) {
			return cursor.getLong(0);
		}
		return -1;
	}

	private void deleteAllLocalCalendarEvents(){
	       ContentResolver cr = getContentResolver();
	       ContentValues values = new ContentValues();
	       Uri deleteUri = null;
	       deleteUri = ContentUris.withAppendedId(Events.CONTENT_URI, getLocalCalendarId());
	       int rows = getContentResolver().delete(deleteUri, null, null);
	}
	
	private void setUpClassCalendar(ArrayList<ParseObject> courseList) {

		// Referring
		// http://www.grokkingandroid.com/androids-calendarcontract-provider/
		ContentValues values = new ContentValues();
		Uri uri;
		if (getLocalCalendarId() == -1) {
			String[] projection = new String[] { Calendars._ID, Calendars.NAME,
					Calendars.ACCOUNT_NAME, Calendars.ACCOUNT_TYPE };
			Cursor calCursor = getContentResolver().query(
					Calendars.CONTENT_URI, projection,
					Calendars.VISIBLE + " = 1", null, Calendars._ID + " ASC");
			if (calCursor.moveToFirst()) {
				do {
					long id = calCursor.getLong(0);
					String displayName = calCursor.getString(1);
					// ...
				} while (calCursor.moveToNext());
			}

			values.put(Calendars.ACCOUNT_NAME, accountName);
			values.put(Calendars.ACCOUNT_TYPE,
					CalendarContract.ACCOUNT_TYPE_LOCAL);
			values.put(Calendars.NAME, "Class Schedules");
			values.put(Calendars.CALENDAR_DISPLAY_NAME, "Class Schedules");
			values.put(Calendars.CALENDAR_COLOR, 0xffff0000);
			values.put(Calendars.CALENDAR_ACCESS_LEVEL,
					Calendars.CAL_ACCESS_OWNER);
			values.put(Calendars.OWNER_ACCOUNT, accountName);

			values.put(Calendars.SYNC_EVENTS, 1);
			Uri.Builder builder = CalendarContract.Calendars.CONTENT_URI
					.buildUpon();
			builder.appendQueryParameter(Calendars.ACCOUNT_NAME,
					"com.wassup49ers");
			builder.appendQueryParameter(Calendars.ACCOUNT_TYPE,
					CalendarContract.ACCOUNT_TYPE_LOCAL);
			builder.appendQueryParameter(
					CalendarContract.CALLER_IS_SYNCADAPTER, "true");
			uri = getContentResolver().insert(builder.build(), values);
		}

		long calId = getLocalCalendarId();
		if (calId == -1) {
			// no calendar account; react meaningfully
			return;
		}

		for (ParseObject obj : courseList) {
			String startTime = obj.getString("classStartTime");
			String endTime = obj.getString("classEndTime");
			String dayOfWeek = getDayOfWeekString(obj.getInt("dayOfWeek"));

			Date eventStartDate = obj.getDate("startDate");
			Date eventEndDate = obj.getDate("endDate");

			Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("EST"));

			// long timeInMillis = cal.getTimeInMillis();
			// ContentValues values = new ContentValues();
			values.clear();
			long timeInMillis;
			cal.setTime(eventStartDate);
			timeInMillis = cal.getTimeInMillis();
			values.put(Events.DTSTART, timeInMillis);

			cal.setTime(eventEndDate);
			// timeInMillis = cal.getTimeInMillis();
			// values.put(Events.DTEND, timeInMillis);
			SimpleDateFormat yyyyMMdd = new SimpleDateFormat("yyyyMMdd");
			Calendar dt = Calendar.getInstance(TimeZone.getTimeZone("EST"));

			// where untilDate is a date instance of your choice,for example
			// 30/01/2012
			dt.setTime(eventEndDate);

			// if you want the event until 30/01/2012 we add one day from our
			// day because UNTIL in RRule sets events Before the last day want
			// for event
			dt.add(Calendar.DATE, 1);
			String dtUntill = yyyyMMdd.format(dt.getTime());

			// by default all class duration is 3 hours
			values.put(Events.DURATION, "+P3H");
			String rruleString = "FREQ=WEEKLY;BYDAY=" + dayOfWeek + ";UNTIL="
					+ dtUntill;
			values.put(Events.RRULE, rruleString);

			values.put(Events.TITLE, obj.getString("courseName") + "Class");
			values.put(Events.EVENT_LOCATION, obj.getString("location"));
			values.put(Events.CALENDAR_ID, calId);
			values.put(Events.EVENT_TIMEZONE, TimeZone.getTimeZone("EST").getID());
			values.put(Events.DESCRIPTION, "");
			// reasonable defaults exist:
			values.put(Events.ACCESS_LEVEL, Events.ACCESS_PRIVATE);
			values.put(Events.SELF_ATTENDEE_STATUS, Events.STATUS_CONFIRMED);
			values.put(Events.ALL_DAY, 0);
			// values.put(Events.ORGANIZER, "some.mail@some.address.com");
			// values.put(Events.GUESTS_CAN_INVITE_OTHERS, 1);
			// values.put(Events.GUESTS_CAN_MODIFY, 1);
			values.put(Events.AVAILABILITY, Events.AVAILABILITY_BUSY);
			uri = getContentResolver().insert(Events.CONTENT_URI, values);
			long eventId = new Long(uri.getLastPathSegment());

			// adding an attendee:
			values.clear();
			values.put(Attendees.EVENT_ID, eventId);
			values.put(Attendees.ATTENDEE_TYPE, Attendees.TYPE_REQUIRED);
			values.put(Attendees.ATTENDEE_NAME, app.getUserName());
			values.put(Attendees.ATTENDEE_EMAIL, ParseUser.getCurrentUser()
					.getEmail());
			getContentResolver().insert(Attendees.CONTENT_URI, values);
			// adding a reminder:
			values.clear();
			values.put(Reminders.EVENT_ID, eventId);
			values.put(Reminders.METHOD, Reminders.METHOD_ALERT);
			values.put(Reminders.MINUTES, 30);
			getContentResolver().insert(Reminders.CONTENT_URI, values);
		}
		Toast.makeText(this, "Your calendar is synced with you course timings. " +
				"You will be notified before you class begins", Toast.LENGTH_LONG).show();

	}

	private String getDayOfWeekString(int day) {
		String dayString = null;
		switch (day) {
		case 0:
			dayString = "SU";
			break;
		case 1:
			dayString = "MO";
			break;
		case 2:
			dayString = "TU";
			break;
		case 3:
			dayString = "WE";
			break;
		case 4:
			dayString = "TH";
			break;
		case 5:
			dayString = "FR";
			break;
		default:
			break;
		}
		return dayString;
	}
	
	public void onCourseWorkClicked(View v){
		Intent intent = new Intent(StudentMenuActivity.this,
				CourseWorkActivity.class);
		startActivity(intent);
		
	}
	public void onEmergencyClicked(View v){
		Intent intent = new Intent(StudentMenuActivity.this,
				EmergencyActivity.class);
		startActivity(intent);
		
		
	}
	public void onPostToFacebookClicked(View v){
		Intent intent = new Intent(StudentMenuActivity.this,
				FacebookLoginActivity.class);
		startActivity(intent);
		
	}
	public void onSyncCourseCalendarClicked(View v){
		setUpClassCalendar(courseList);
		
	}
}
