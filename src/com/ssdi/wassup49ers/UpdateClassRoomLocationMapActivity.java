package com.ssdi.wassup49ers;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMapClickListener;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

public class UpdateClassRoomLocationMapActivity extends Activity {
	GoogleMap gMap;
	LatLng classRoomLocation;
	Marker classMarker;
	LatLng currentSelectedLocation;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_update_classroom_location_map);
		
		
		double latitude = getIntent().getExtras().getDouble("latitude_key");
		double longitude = getIntent().getExtras().getDouble("longitude_key");
		final String markerInfoString = getIntent().getExtras().getString("marker_info_key");
		
		classRoomLocation = new LatLng(latitude, longitude);
		currentSelectedLocation = classRoomLocation;
		

		
		setUpMapIfNeeded();
		

		gMap.setOnMapClickListener(new OnMapClickListener() {
			
			@Override
			public void onMapClick(LatLng arg0) {
				if(classMarker != null){
					classMarker.remove();
				}
				currentSelectedLocation = arg0;
				classMarker = gMap.addMarker(new MarkerOptions()
				.position(currentSelectedLocation)
				.title(markerInfoString));
			}
		});
		
		classMarker = gMap.addMarker(new MarkerOptions()
		.position(classRoomLocation)
		.title(markerInfoString));
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.update_classroom, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	private LatLngBounds.Builder getZooomedLatLngBoundsForGeoPoints(ArrayList<LatLng> points){
	    LatLngBounds.Builder latLngBounds = new LatLngBounds.Builder();

	    for (LatLng item : points) {
	        latLngBounds.include(item);
	    }
	    return latLngBounds;
	}
	private void setUpMapIfNeeded() {
	    // Do a null check to confirm that we have not already instantiated the map.
	    if (gMap == null) {
	    	gMap = ((MapFragment) getFragmentManager().findFragmentById(R.id.map))
	                            .getMap();
			final ArrayList<LatLng> classroomLocationArrayList = new ArrayList<LatLng>();
			classroomLocationArrayList.add(classRoomLocation);
			
			gMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
				@Override
				public void onMapLoaded() {
					// zoom and pan the map to include all the place points
					gMap.moveCamera(CameraUpdateFactory.newLatLngBounds(
							getZooomedLatLngBoundsForGeoPoints(classroomLocationArrayList)
									.build(), 20));
				}
			});

	    }
	}
	
	public void onDoneButtonClicked(View v){
		Intent intent = new Intent();
		intent.putExtra("latitude_key", currentSelectedLocation.latitude);
		intent.putExtra("longitude_key", currentSelectedLocation.longitude);
		//￼intent.putExtra(UpdateClassroomLocationActivity.RESULT_KEY, "Hello World !!");
		setResult(RESULT_OK, intent);
		finish();
	}
}
