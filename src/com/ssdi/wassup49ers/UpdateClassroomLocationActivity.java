package com.ssdi.wassup49ers;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseGeoPoint;
import com.parse.ParseInstallation;
import com.parse.ParseObject;
import com.parse.ParsePush;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;

public class UpdateClassroomLocationActivity extends Activity {
	final static int REQ_CODE = 1001;

	ParseUser currentUser;
	Wassup49ersApplication app;
	ArrayList<String> courseNames;
	ArrayList<ParseObject> courseList;
	ParseObject currentSelectedCourse;
	ArrayList<String> courseIDs;
	ArrayList<String> courseLocationList;
	EditText classLocationEt;
	TextView classLocationCoordinatesTv;
	ParseGeoPoint classLocationCoordinates;
	String classLocation;
	
	ParseGeoPoint newClassLocation;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_update_classroom_location);

		app = (Wassup49ersApplication) getApplication();
		currentUser = ParseUser.getCurrentUser();
		classLocationEt = (EditText) findViewById(R.id.editText1);
		classLocationCoordinatesTv = (TextView) findViewById(R.id.textView5);

		ParseQuery<ParseObject> query = ParseQuery.getQuery("Faculty");
		query.whereEqualTo("userID", currentUser);

		// Log.d("demo", currentUser.getObjectId());
		query.findInBackground(new FindCallback<ParseObject>() {

			@Override
			public void done(List<ParseObject> objects, ParseException e) {

				ParseObject facultyObj = (ParseObject) objects.get(0);

				// TO DO: Column should be renamed to "courses" in parse
				courseIDs = (ArrayList<String>) facultyObj.get("Courses");

				ParseQuery<ParseObject> query1 = ParseQuery.getQuery("Course");
				query1.whereContainedIn("objectId", courseIDs);
				query1.findInBackground(new FindCallback<ParseObject>() {
					@Override
					public void done(List<ParseObject> objects, ParseException e) {
						courseList = new ArrayList<ParseObject>();
						courseNames = new ArrayList<String>();
						courseLocationList = new ArrayList<String>();
						for (ParseObject obj : objects) {
							Log.d("demo", obj.toString());
							courseNames.add((String) obj.get("courseName"));
							courseLocationList.add(obj.getString("location"));
							courseList.add(obj);
						}

						Spinner courseSpinner = (Spinner) findViewById(R.id.spinner1);

						ArrayAdapter<String> adapter = new ArrayAdapter<String>(
								UpdateClassroomLocationActivity.this,
								android.R.layout.simple_spinner_item,
								courseNames);

						currentSelectedCourse = courseList.get(0);

						classLocationEt.setText(courseList.get(0).getString(
								"location"));
						classLocationCoordinatesTv.setText(courseList.get(0)
								.get("gpsLocation").toString());

						// Specify the layout to use when the list of choices
						// appears
						adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
						// Apply the adapter to the spinner
						courseSpinner.setAdapter(adapter);

						courseSpinner
								.setOnItemSelectedListener(new OnItemSelectedListener() {

									@Override
									public void onItemSelected(
											AdapterView<?> parent, View view,
											int position, long id) {

										// ParseQuery<ParseObject> query =
										// ParseQuery
										// .getQuery("Group");
										Log.d("demo",
												"Course ID :"
														+ courseIDs
																.get(position));

										app.setCurrentCourse(courseNames
												.get(position));
										currentSelectedCourse = courseList
												.get(position);
										classLocationEt
												.setText(courseList.get(
														position).getString(
														"location"));

										classLocation = courseList.get(
												position).getString(
												"location");
										classLocationCoordinates = (ParseGeoPoint) courseList
												.get(position).get(
														"gpsLocation");
										classLocationCoordinatesTv.setText(classLocationCoordinates
												.getLatitude()
												+ ", "
												+ classLocationCoordinates.getLongitude());

										// currentCourse = courseList
										// .get(position);
										// query.whereEqualTo("courseId",
										// currentCourse);
									}

									@Override
									public void onNothingSelected(
											AdapterView<?> parent) {
										// TODO Auto-generated method stub

									}
								});

					}
				});
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.update_classroom_location, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	public void onUpdateClassroomClicked(View v) {
		
		//if((classLocationCoordinates.getLatitude() != newClassLocation.getLatitude()) || (classLocationCoordinates.getLongitude() != newClassLocation.getLongitude()) || !classLocation.equals(classLocationEt.getText().toString())){
			if(newClassLocation == null)
				newClassLocation = classLocationCoordinates;
			currentSelectedCourse.put("gpsLocation", newClassLocation);
			currentSelectedCourse.put("location", classLocationEt.getText().toString());
			currentSelectedCourse.saveInBackground(new SaveCallback() {

				@Override
				public void done(ParseException e) {
					// TODO Auto-generated method stub
					Log.d("demo", "Classroom Loocation Updated");
					classLocationCoordinatesTv.setText(newClassLocation
							.getLatitude()
							+ ", "
							+ newClassLocation.getLongitude());

					publishClassRoomChange();
					
					Toast.makeText(getBaseContext(), "Classroom location updated", Toast.LENGTH_LONG).show();
					finish();
				}
			});
		//}
		

	}
	
	public void onChangeMapLocationClicked(View v) {
		Intent intent = new Intent(UpdateClassroomLocationActivity.this,
				UpdateClassRoomLocationMapActivity.class);
		intent.putExtra("latitude_key", classLocationCoordinates.getLatitude());
		intent.putExtra("longitude_key", classLocationCoordinates.getLongitude());
		intent.putExtra("marker_info_key", currentSelectedCourse.getString("courseName"));
		startActivityForResult(intent, REQ_CODE);
	}

	
	
	private void publishClassRoomChange(){
		//notify all students about classroom change
		//step 1 get the list of students for the particular course
		
		ArrayList<String> courseQueryCollection = new ArrayList<String>();
		courseQueryCollection.add(currentSelectedCourse.getObjectId());
		ParseQuery<ParseObject> query = ParseQuery.getQuery("Student");
		query.whereContainedIn("courses", courseQueryCollection);
		query.findInBackground(new FindCallback<ParseObject>() {

			@Override
			public void done(List<ParseObject> objects, ParseException e) {
				if (objects.size() > 0){
					Wassup49ersApplication app = (Wassup49ersApplication) getApplication();
					Log.d("demo", "Found Students");
					ArrayList<ParseUser> studentList = new ArrayList<ParseUser>();
					for (ParseObject obj:objects){
						//Log.d("demo", obj.getParseUser("userId").getString("userFullName"));
						studentList.add(obj.getParseUser("userId"));
					}
					ParsePush parsePush = new ParsePush();
					ParseQuery pQuery = ParseInstallation.getQuery(); // <-- Installation query
					pQuery.whereContainedIn("user", studentList); // <-- you'll probably want to target someone that's not the current user, so modify accordingly
					parsePush.sendMessageInBackground(app.getUserName()+" has updated location for "+currentSelectedCourse.getString("courseName"), pQuery);
				}
				
			}
		});
		
		
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == REQ_CODE) {
			if (resultCode == RESULT_OK
					&& data.getExtras().containsKey("latitude_key")
					&& data.getExtras().containsKey("longitude_key")) {
				  newClassLocation = new ParseGeoPoint();
				newClassLocation.setLatitude(data.getExtras().getDouble(
						"latitude_key"));
				newClassLocation.setLongitude(data.getExtras().getDouble(
						"longitude_key"));
				
				classLocationCoordinatesTv.setText(newClassLocation
						.getLatitude()
						+ ", "
						+ newClassLocation.getLongitude());


			} else {

				Toast.makeText(
						this,
						"No update done",
						Toast.LENGTH_LONG).show();
			}
		}
	}
}
