package com.ssdi.wassup49ers;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.provider.CalendarContract;
import android.provider.CalendarContract.Attendees;
import android.provider.CalendarContract.Events;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseGeoPoint;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.ssdi.wassup49ers.ScheduleMeeting.MeetingSlot;

public class ViewSlotsActivity extends Activity {

	Date startDate;
	Date endDate;
	int duration;

	ArrayList<MeetingSlot> meetingSlots;
	ArrayList<String> meetingSlotsDurationStringList;

	ProgressDialog progressDialog;

	Wassup49ersApplication app;
	ArrayList<String> emailAddresses;
	ListView listView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_view_slots);

		progressDialog = new ProgressDialog(this);
		app = (Wassup49ersApplication) getApplication();
		listView = (ListView) findViewById(R.id.ListView1);
		startDate = new Date();
		startDate.setTime(getIntent().getLongExtra("startDate", -1));

		endDate = new Date();
		endDate.setTime(getIntent().getLongExtra("endDate", -1));

		duration = getIntent().getIntExtra("duration", -1);

		meetingSlots = calculateMeetingSlots();
		meetingSlotsDurationStringList = new ArrayList<String>();

		for (MeetingSlot ms : meetingSlots) {
			Log.d("demo", ms.toString());
			meetingSlotsDurationStringList.add(ms.toString());
		}

		getGroupMembersAccountsForUser(ParseUser.getCurrentUser());

	}

	public ArrayList<MeetingSlot> calculateMeetingSlots() {
		long durationinMs = duration * 1000 * 60;
		ArrayList<MeetingSlot> meetingSlots = new ArrayList<MeetingSlot>();
		long endMs = endDate.getTime();
		for (long startMs = startDate.getTime(); startMs < endMs; startMs = startMs
				+ durationinMs) {
			meetingSlots.add(new MeetingSlot(new Date(startMs), new Date(
					startMs + durationinMs)));
		}
		return meetingSlots;
	}

	private void getGroupMembersAccountsForUser(ParseUser user) {
		showProgress("Loading group member data");
		emailAddresses = new ArrayList<String>();
		ParseQuery<ParseObject> query = ParseQuery.getQuery("Group");
		query.whereEqualTo("groupName", app.getCurrentGroup());
		query.getFirstInBackground(new GetCallback<ParseObject>() {

			@Override
			public void done(ParseObject object, ParseException e) {
				ArrayList<String> groupMembers = (ArrayList<String>) object
						.get("members");

				ParseQuery query1 = ParseUser.getQuery();
				query1.whereContainedIn("objectId", groupMembers);
				query1.findInBackground(new FindCallback<ParseUser>() {

					@Override
					public void done(List<ParseUser> objects, ParseException e) {

						ParseQuery<ParseObject> innerQuery = ParseQuery
								.getQuery("Calendar");
						innerQuery.whereContainedIn("userId", objects);
						innerQuery
								.findInBackground(new FindCallback<ParseObject>() {

									@Override
									public void done(List<ParseObject> objects,
											ParseException e) {
										// TODO Auto-generated method stub
										if ((objects != null)
												&& (objects.size() > 0)) {
											for (ParseObject obj : objects) {
												emailAddresses.add((String) obj
														.get("googleAccount"));
											}
										}

										hideProgress();

										ArrayAdapter<String> meetingSlotsDurationAdapter = new ArrayAdapter<String>(
												ViewSlotsActivity.this,
												android.R.layout.simple_list_item_1,
												meetingSlotsDurationStringList);
										listView.setAdapter(meetingSlotsDurationAdapter);
										meetingSlotsDurationAdapter
												.setNotifyOnChange(true);

										listView.setOnItemClickListener(new OnItemClickListener() {

											@Override
											public void onItemClick(
													AdapterView<?> parent,
													View view, int position,
													long id) {
												// TODO Auto-generated method
												// stub
												Log.d("demo", "item clicked");
												String emailAddressString = "";
												if (emailAddresses.size() > 0) {
													for (String email : emailAddresses) {
														emailAddressString += email;
														emailAddressString += ",";
													}

													emailAddressString = emailAddressString
															.substring(
																	0,
																	emailAddressString
																			.length() - 1);
												}
												Intent calIntent = new Intent(
														Intent.ACTION_INSERT);
												calIntent
														.setType("vnd.android.cursor.item/event");
												calIntent.putExtra(
														Events.TITLE,
														app.getCurrentCourse()
																+ " "
																+ app.getCurrentGroup()
																+ " Group Meet");
												calIntent.putExtra(
														Events.EVENT_LOCATION,
														"Woodward");
												calIntent.putExtra(
														Events.DESCRIPTION,
														app.getCurrentCourse()
																+ " "
																+ app.getCurrentGroup()
																+ " Group Meet");
												calIntent.putExtra(
														Intent.EXTRA_EMAIL,
														emailAddressString);

												Calendar cal = Calendar
														.getInstance();
												cal.setTime(meetingSlots.get(
														position)
														.getStartDate());

												calIntent
														.putExtra(
																CalendarContract.EXTRA_EVENT_BEGIN_TIME,
																cal.getTimeInMillis());
												cal.setTime(meetingSlots.get(
														position).getEndDate());

												calIntent
														.putExtra(
																CalendarContract.EXTRA_EVENT_END_TIME,
																cal.getTimeInMillis());

												startActivity(calIntent);

											}
										});
									}
								});

					}
				});

			}
		});

	}

	private void showProgress(String message) {
		progressDialog.setMessage(message);
		progressDialog.setCancelable(false);
		progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		progressDialog.show();
	}

	private void hideProgress() {
		progressDialog.dismiss();
	}

}
