package com.ssdi.wassup49ers;

import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParsePush;
import com.parse.SaveCallback;

import android.app.Application;
import android.location.LocationListener;
import android.location.LocationManager;
import android.util.Log;

public class Wassup49ersApplication extends Application{
	@Override
	public void onCreate() {
		Parse.initialize(this, "hMPbrSOa8LmaY2J3veeq5KDtWVsVIgt7SCDIIZVU",
				"2xJS8hPBKBFDYeDjB4wWDlA9Ady55Nwyfmm4elZ6");
		ParsePush.subscribeInBackground("", new SaveCallback() {
			  @Override
			  public void done(ParseException e) {
			    if (e == null) {
			      Log.d("com.parse.push", "successfully subscribed to the broadcast channel.");
			    } else {
			      Log.e("com.parse.push", "failed to subscribe for push", e);
			    }
			  }
			});
		
		super.onCreate();
		
	}
	private String currentCourse;
	private String currentGroup;
	private String googleAccount;
	private LocationManager mLocationMngr;
	private LocationListener mLocListener;
	private String userName;
	
	
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public LocationManager getLocationMngr() {
		return mLocationMngr;
	}
	public void setLocationMngr(LocationManager mLocationMngr) {
		this.mLocationMngr = mLocationMngr;
	}
	public LocationListener getLocListener() {
		return mLocListener;
	}
	public void setLocListener(LocationListener mLocListener) {
		this.mLocListener = mLocListener;
	}
	public String getGoogleAccount() {
		return googleAccount;
	}
	public void setGoogleAccount(String googleAccount) {
		this.googleAccount = googleAccount;
	}
	public String getCurrentCourse() {
		return currentCourse;
	}
	public void setCurrentCourse(String currentCourse) {
		this.currentCourse = currentCourse;
	}
	public String getCurrentGroup() {
		return currentGroup;
	}
	public void setCurrentGroup(String currentGroup) {
		this.currentGroup = currentGroup;
	}
	
	

}
